<?php

define('point_depart', '2,4');
define('point_arrivee', '1,0');

define('points', [
    '2,1' => [
        '2,2' => 1,
        '1,1' => 1,
        '3,1'=>1,
    ],
    '2,2' => [
        '3,2' => 1,
        '2,1' => 1,
    ],
    '3,1' =>[
        '3,2' => 1,
        '4,1' => 1,
        '2,1' =>1,
    ],
    '3,2' => [
        '3,3' => 1,
        '2,2' => 1,
        '3,1' =>1,
    ],
    '3,3' => [
        '3,4' => 1,
        '3,2' => 1,
        '4,3' =>1,
    ],
    '3,4' => [
        '2,4' => 1,
        '3,3' => 1,
        '4,4' =>1,
    ],
    '2,4' => [
        '3,4' => 1,
        '1,4' => 1,
    ],
    '1,4' => [
        '2,4' => 1,
        '0,4' => 1,
    ],
    '0,4' => [
        '1,4' => 1,
        '0,3' => 1,
    ],
    '0,3' => [
        '0,4' => 1,
        '0,2' => 1,
    ],
    '0,2' => [
        '0,3' => 1,
        '0,1' => 1,
    ],
    '0,1' => [
        '0,2' => 1,
        '0,0' => 1,
        '1,1' => 1
    ],
    '1,1' => [
        '0,1' => 1,
        '1,0' => 1,
        '2,1' => 1
    ],
    '1,0' => [
        '1,1' => 1,
        '0,0' => 1
    ],
    '0,0' => [
        '1,0' => 1,
        '0,1' => 1
    ]
]);






$points = [];
foreach (points as $point => $points_liees) {
    $points[$point] = [
        'distance_du_depart' => 999999999,
        'routes' => $points_liees,
        'chemin' => []
    ];
}
$points[point_depart]['distance_du_depart'] = 0;


//  comparaison des distances 

$comparaison = function (array $a, array $b): int {
    if ($a['distance_du_depart'] < $b['distance_du_depart']) {
        return -1;
    }

    return 1;
};


// Algo de Dijkstra
$chemin = [];

while ($points) {
    uasort($points, $comparaison);


    // On récupère la premiere clé car c'est le point avec la plus faible distance
    $point_parent = array_keys($points)[0];
    $point_parent_routes = $points[$point_parent]['routes'];
    $point_parent_distance = $points[$point_parent]['distance_du_depart'];
    $point_parent_distance;
    $point_parent_chemin = $points[$point_parent]['chemin'];

  
    $point_parent_chemin[] = $point_parent;


    unset($points[$point_parent]);
    
 

    // On sort de la boucle si aucun chemin ne mène à destination
    if ($point_parent_distance == 999999999) {
        break;
    }

    //  La destination est le point avec la plus faible distance 
    if ($point_parent == point_arrivee) {
        $chemin = ['distance' => $point_parent_distance, 'points' => $point_parent_chemin];
        


        break;
    }


    // On boucle pour voir si on est au plus prêt des points voisins par rapport au point départ.
    // Si oui, on y assigne la liste des points et la distance du chemin emprunté depuis le point de départ
    foreach ($point_parent_routes as $point_voisin => $distance) {
        $distance_du_depart = $point_parent_distance + $distance;

        if (!isset($points[$point_voisin]) || $points[$point_voisin]['distance_du_depart'] < $distance_du_depart) {
            continue;
        }

        $points[$point_voisin]['distance_du_depart'] = $distance_du_depart;
        $points[$point_voisin]['chemin'] = $point_parent_chemin;
 
    }
}

if (!$chemin) {
    echo '<p>Aucun chemin n\'est possible entre le point de départ et celui d\'arrivée.</p>';
} else {
    echo "<p>Le (ou l'un des) chemin(s) le(s) plus court(s) est de " . $chemin['distance'] . 'point(s)  : ' . implode(' => ', $chemin['points']);
}